namespace SimpleCalculator
{
    public class SummarizeCommand
    {
        private double _first;

        private double _second;

        public SummarizeCommand(double first, double second)
        {
            this._first = first;
            this._second = second;
        }

        public void Execute(ref double result)
        {
            result = this._first + this._second;
        }
    }
}