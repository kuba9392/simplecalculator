using System;
using SimpleCalculator;
using Xunit;

namespace SimpleCalculatorTests
{
    public class SummarizeCommandUnitTest
    {
        [Fact]
        public void TestExecute()
        {
            SummarizeCommand command = new SummarizeCommand(1, 2);

            double result = 0;
            command.Execute(ref result);

            Assert.Equal(3, result);
        }
    }
}